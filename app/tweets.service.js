System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var TweetsService;
    return {
        setters:[],
        execute: function() {
            TweetsService = (function () {
                function TweetsService() {
                }
                TweetsService.prototype.getTweets = function () {
                    return [
                        {
                            imageUrl: "http://lorempixel.com/100/100/people?1",
                            author: "FawzyTat",
                            handle: "@Hashtat",
                            body: " One life , Do something ",
                            totalLikes: 12,
                            iLike: false
                        },
                        {
                            imageUrl: "http://lorempixel.com/100/100/people?2",
                            author: "Sabry Ragab",
                            handle: "@sabryelprince",
                            body: " If it is wrong but you have to do it , Do it right ! ",
                            totalLikes: 17,
                            iLike: true
                        },
                        {
                            imageUrl: "http://lorempixel.com/100/100/people?3",
                            author: " Mina william ",
                            handle: "@minaaa",
                            body: "calm is good  ",
                            totalLikes: 14,
                            iLike: true
                        }];
                };
                return TweetsService;
            }());
            exports_1("TweetsService", TweetsService);
        }
    }
});
//# sourceMappingURL=tweets.service.js.map