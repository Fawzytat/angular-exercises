import {Component, Input} from 'angular2/core'

@Component ({

  selector:'likes' ,
  template:
  `

  <h4><i class="glyphicon glyphicon-heart"
    [class.highlighted]="ilike"
     (click) ="onClick()"  >
  </i>
    <span> {{ totallikes }} </span></h4>
  `
  ,
  styles: [
    `
    .glyphicon-heart {
      color : #ccc ;
      cursor : pointer ;
    }
    .highlighted {
      color : red ;

    }



    `
  ]

})

export class LikeComponent {
  @Input() totallikes = 0;
  @Input() ilike = false;
  onClick(){
    this.ilike = !this.ilike ;
    this.totallikes += this.ilike ? 1 : -1;
  }
}
