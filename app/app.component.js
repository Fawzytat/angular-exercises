System.register(['angular2/core', './courses.component', './authors.component', './favTest.component', './like.component', './voter.component', './twitter.component', './controllers.component', './CustomPipes.component', './bootstrap.panel.component', './zippy.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, courses_component_1, authors_component_1, favTest_component_1, like_component_1, voter_component_1, twitter_component_1, controllers_component_1, CustomPipes_component_1, bootstrap_panel_component_1, zippy_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (courses_component_1_1) {
                courses_component_1 = courses_component_1_1;
            },
            function (authors_component_1_1) {
                authors_component_1 = authors_component_1_1;
            },
            function (favTest_component_1_1) {
                favTest_component_1 = favTest_component_1_1;
            },
            function (like_component_1_1) {
                like_component_1 = like_component_1_1;
            },
            function (voter_component_1_1) {
                voter_component_1 = voter_component_1_1;
            },
            function (twitter_component_1_1) {
                twitter_component_1 = twitter_component_1_1;
            },
            function (controllers_component_1_1) {
                controllers_component_1 = controllers_component_1_1;
            },
            function (CustomPipes_component_1_1) {
                CustomPipes_component_1 = CustomPipes_component_1_1;
            },
            function (bootstrap_panel_component_1_1) {
                bootstrap_panel_component_1 = bootstrap_panel_component_1_1;
            },
            function (zippy_component_1_1) {
                zippy_component_1 = zippy_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                    this.TestText = "Active submit button";
                    this.isActive = "true";
                    this.testtitle = "test title";
                    this.tweet = {
                        totallikes: 10,
                        ilike: false
                    };
                    this.post = {
                        totalVotes: 10,
                        myVote: 0
                    };
                }
                AppComponent.prototype.onVote = function ($event) { console.log($event); };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-6 col-md-offset-3\">\n              <h1>Angular 2 Tutorial exercises | tat  </h1>\n           </div>\n\n        </div>\n\n        <hr />\n\n        <div class=\"row\">\n\n            <div class=\"col-md-3\">\n              <div class=\"panel panel-primary\">\n                  <div class=\"panel-heading\">\n                    <h4 class=\"text-center\"> Courses List </h4>\n                  </div>\n                  <div class=\"panel-body\">\n                    <courses></courses>\n                  </div>\n              </div>\n            </div>\n\n\n          <div class=\"col-md-3\">\n            <div class=\"panel panel-primary\">\n                <div class=\"panel-heading\">\n                  <h4> Authors Exercise</h4>\n                </div>\n                <div class=\"panel-body\">\n                  <authors></authors>\n                </div>\n            </div>\n          </div>\n\n\n            <div class=\"col-md-3\">\n              <div class=\"panel panel-danger\">\n                  <div class=\"panel-heading\">\n                      <h4>{{ TestText }}</h4>\n                  </div>\n                  <div class=\"panel-body\">\n                      <p> One with Class binding | one without </p>\n                      <hr />\n                      <button class=\"btn btn-primary\" [class.Active]=\"isActive\"> Submit </button>\n                      <button class=\"btn btn-primary\"> Submit </button>\n                  </div>\n              </div>\n            </div>\n\n\n            <div class=\"col-md-3\">\n                  <div class=\"panel panel-warning\">\n                    <div class=\"panel-heading\">\n                    <h4> Data binding examples </h4>\n                    </div>\n                    <div class=\"panel-body\">\n                          <span class=\"label label-success\"> One way data binding </span>\n                          <p> From component to DOM </p>\n                          <hr />\n                          <input type=\"text\" [value]=\"testtitle\" class=\"form-control\" />\n                          <hr />\n                          <span class=\"label label-success\"> Two ways data binding </span>\n                          <hr />\n                          <input type=\"text\" [(ngModel)]=\"testtitle\" class=\"form-control\" />\n                          <input type=\"button\" (click)=\"testtitle=''\" value=\"clear\" class=\"btn btn-default\" />\n                            <h5> preview: <span class=\"label label-info\"> {{ testtitle }} </span> </h5>\n\n                      </div>\n                </div>\n            </div>\n\n\n        </div>\n        <hr />\n        <div class=\"row\">\n            <div class=\"well\">\n                <favourits></favourits>\n            </div>\n        </div>\n\n\n        <div class=\"row\">\n            <div class=\"well\">\n                <label class=\"form-label\" > ( Like ) exercise | using class binding and event binding and the Input decorator  </label>\n                <likes [totallikes]=\"tweet.totallikes\" [ilike]=\"tweet.ilike\"></likes>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"well\">\n              <label class=\"form-label\" > ( Votes ) exercise | using class binding and event binding and the Input,Output,Emit decorator  </label>\n              <votes\n              [totalVotes]=\"post.totalVotes\"\n              [myVote]=\"post.myVote\"\n              (vote) =\"onVote($event)\"\n              ></votes>\n            </div>\n        </div>\n\n\n        <hr />\n        <div class=\"row\">\n          <div class=\"panel panel-primary\">\n\n              <div class=\"panel-heading\">\n                <h4> Twitter Challange </h4>\n              </div>\n\n              <div class=\"panel-body\">\n                <twitter>\n                </twitter>\n              </div>\n\n          </div>\n\n        </div>\n\n        <div class=\"row\">\n            <bs-panel>\n                  <div class=\"heading\">\n                            <h4> controllers and ngs  exercises </h4>\n                  </div>\n\n                  <div class=\"body\">\n                          <controllers>\n                          </controllers>\n                  </div>\n            </bs-panel>\n\n          <hr />\n\n          <bs-panel>\n\n                <div class=\"heading\">\n                        <h4> Custom Pipes exercises </h4>\n                </div>\n\n                <div class=\"body\">\n                    <customPipes>\n                    </customPipes>\n                </div>\n\n          </bs-panel>\n              <hr/>\n\n              <bs-panel>\n\n                    <div class=\"heading\">\n                            <h4> Zippy Exercise </h4>\n                    </div>\n\n                    <div class=\"body\">\n                        <zippy title=\"who can see my stuff ?\">\n                            content of who can see my stuff\n                        </zippy>\n\n                        <zippy title=\"who can see my stuff 2 ?\">\n                            content of who can see my stuff 2\n                        </zippy>\n\n                        <zippy title=\"who can see my stuff 3 ?\">\n                            content of who can see my stuff 3\n                        </zippy>\n                    </div>\n\n              </bs-panel>\n\n              <hr/>\n\n\n        </div>\n\n\n\n\n\n\n\n     ",
                        directives: [courses_component_1.CoursesComponent, authors_component_1.AuthorsComponent, favTest_component_1.FavouriteComponent,
                            like_component_1.LikeComponent, voter_component_1.VoterComponent, twitter_component_1.TwitterComponent,
                            controllers_component_1.ControllersComponent, CustomPipes_component_1.CustomPipes, bootstrap_panel_component_1.BootstrapPanel, zippy_component_1.ZippyComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map