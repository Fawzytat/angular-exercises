import {Component} from 'angular2/core';
import {AuthorService} from './author.service'
@Component ({
  selector: 'authors',
  template:
  `

  {{ title }}
  <ul class="list-group">
    <li *ngFor="#author of authors" class="list-group-item">

      {{author}}

    </li>
  </ul>
  `,
  providers : [AuthorService]
})
export class AuthorsComponent {
    title = "The title of the Author page ";
    authors;
    constructor (authorService : AuthorService){
      this.authors = authorService.getAuthors();
    }

}
