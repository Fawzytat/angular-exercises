import {Component} from 'angular2/core';
import {CoursesComponent} from './courses.component';
import {AuthorsComponent} from './authors.component';
import {FavouriteComponent} from './favTest.component';
import {LikeComponent} from './like.component';
import {VoterComponent} from './voter.component';
import {TwitterComponent} from './twitter.component';
import {ControllersComponent} from './controllers.component';
import {CustomPipes} from './CustomPipes.component';
import {BootstrapPanel} from './bootstrap.panel.component';
import {ZippyComponent} from './zippy.component';
@Component ({
    selector: 'my-app',
    template: `
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h1>Angular 2 Tutorial exercises | tat  </h1>
           </div>

        </div>

        <hr />

        <div class="row">

            <div class="col-md-3">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h4 class="text-center"> Courses List </h4>
                  </div>
                  <div class="panel-body">
                    <courses></courses>
                  </div>
              </div>
            </div>


          <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <h4> Authors Exercise</h4>
                </div>
                <div class="panel-body">
                  <authors></authors>
                </div>
            </div>
          </div>


            <div class="col-md-3">
              <div class="panel panel-danger">
                  <div class="panel-heading">
                      <h4>{{ TestText }}</h4>
                  </div>
                  <div class="panel-body">
                      <p> One with Class binding | one without </p>
                      <hr />
                      <button class="btn btn-primary" [class.Active]="isActive"> Submit </button>
                      <button class="btn btn-primary"> Submit </button>
                  </div>
              </div>
            </div>


            <div class="col-md-3">
                  <div class="panel panel-warning">
                    <div class="panel-heading">
                    <h4> Data binding examples </h4>
                    </div>
                    <div class="panel-body">
                          <span class="label label-success"> One way data binding </span>
                          <p> From component to DOM </p>
                          <hr />
                          <input type="text" [value]="testtitle" class="form-control" />
                          <hr />
                          <span class="label label-success"> Two ways data binding </span>
                          <hr />
                          <input type="text" [(ngModel)]="testtitle" class="form-control" />
                          <input type="button" (click)="testtitle=''" value="clear" class="btn btn-default" />
                            <h5> preview: <span class="label label-info"> {{ testtitle }} </span> </h5>

                      </div>
                </div>
            </div>


        </div>
        <hr />
        <div class="row">
            <div class="well">
                <favourits></favourits>
            </div>
        </div>


        <div class="row">
            <div class="well">
                <label class="form-label" > ( Like ) exercise | using class binding and event binding and the Input decorator  </label>
                <likes [totallikes]="tweet.totallikes" [ilike]="tweet.ilike"></likes>
            </div>
        </div>

        <div class="row">
            <div class="well">
              <label class="form-label" > ( Votes ) exercise | using class binding and event binding and the Input,Output,Emit decorator  </label>
              <votes
              [totalVotes]="post.totalVotes"
              [myVote]="post.myVote"
              (vote) ="onVote($event)"
              ></votes>
            </div>
        </div>


        <hr />
        <div class="row">
          <div class="panel panel-primary">

              <div class="panel-heading">
                <h4> Twitter Challange </h4>
              </div>

              <div class="panel-body">
                <twitter>
                </twitter>
              </div>

          </div>

        </div>

        <div class="row">
            <bs-panel>
                  <div class="heading">
                            <h4> controllers and ngs  exercises </h4>
                  </div>

                  <div class="body">
                          <controllers>
                          </controllers>
                  </div>
            </bs-panel>

          <hr />

          <bs-panel>

                <div class="heading">
                        <h4> Custom Pipes exercises </h4>
                </div>

                <div class="body">
                    <customPipes>
                    </customPipes>
                </div>

          </bs-panel>
              <hr/>

              <bs-panel>

                    <div class="heading">
                            <h4> Zippy Exercise </h4>
                    </div>

                    <div class="body">
                        <zippy title="who can see my stuff ?">
                            content of who can see my stuff
                        </zippy>

                        <zippy title="who can see my stuff 2 ?">
                            content of who can see my stuff 2
                        </zippy>

                        <zippy title="who can see my stuff 3 ?">
                            content of who can see my stuff 3
                        </zippy>
                    </div>

              </bs-panel>

              <hr/>


        </div>







     ` ,
    directives: [CoursesComponent,AuthorsComponent,FavouriteComponent,
      LikeComponent,VoterComponent,TwitterComponent,
      ControllersComponent,CustomPipes,BootstrapPanel,ZippyComponent]
})
export class AppComponent {

TestText = "Active submit button";
isActive = "true";


testtitle ="test title";
tweet = {
  totallikes:10,
  ilike:false
}

post ={
  totalVotes : 10,
  myVote :0
}
onVote($event){ console.log($event); }

}
