System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ControllersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ControllersComponent = (function () {
                function ControllersComponent() {
                    this.courses = [
                        {
                            title: " Data structure ",
                            rating: 43.32,
                            students: 5435,
                            price: 99.96,
                            releaseDate: new Date(2016, 5, 12)
                        },
                        {
                            title: "Logic Design",
                            rating: 443,
                            students: 523435,
                            price: 645326.43,
                            releaseDate: new Date(2016, 3, 10)
                        }
                    ];
                    this.viewMode = "map";
                }
                ControllersComponent = __decorate([
                    core_1.Component({
                        selector: 'controllers',
                        template: "\n    <h4> showing list using if statement   </h4>\n    <div *ngIf=\"courses.length > 0\" >\n      List of courses\n    </div>\n\n    <div *ngIf=\"courses.length == 0\" >\n      You don't have any courses\n    </div>\n\n    <hr>\n    <h4> showing list using Switch  </h4>\n    <div>\n        <ul  class=\"nav nav-pills\" >\n          <li [class.active] = \"viewMode =='map'\"> <a (click)=\"viewMode = 'map'\"> Map view </a></li>\n          <li [class.active] = \"viewMode =='list'\"> <a (click)=\"viewMode = 'list'\"> List view </a></li>\n        </ul>\n    </div>\n\n    <div [ngSwitch] =\"viewMode\">\n        <template [ngSwitchWhen] =\"'map'\" ngSwitchDefault> Map view content  </template>\n        <template  [ngSwitchWhen] =\"'list'\"> List view content </template>\n    </div>\n\n    <hr>\n    <h4> Using pipes and for loop  </h4>\n      <ul *ngFor=\"#course of courses\">\n          <li> {{ course.title | uppercase }} </li>\n          <li> {{ course.rating | number }} </li>\n          <li> {{ course.students | number:'2.2-2' }} </li>\n          <li> {{ course.price }} </li>\n          <li> {{ course.releaseDate | date:'MMM yyyy' }} </li>\n          <li> {{ course | json }} </li>\n\n\n      </ul>\n\n   "
                    }), 
                    __metadata('design:paramtypes', [])
                ], ControllersComponent);
                return ControllersComponent;
            }());
            exports_1("ControllersComponent", ControllersComponent);
        }
    }
});
//# sourceMappingURL=controllers.component.js.map