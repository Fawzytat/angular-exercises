import {Component} from 'angular2/core'

@Component ({

  selector: 'favourits',
  template:
  `
  <label class="form-label"> ( Favourite ) exercise | using class binding and event binding  </label>
  <h4> <i  class="glyphicon"
          [class.glyphicon-star-empty]="!isFavourite"
          [class.glyphicon-star]="isFavourite"
          (click)="onClick()">
          </i> </h4>

  `



})

export class FavouriteComponent {

isFavourite= false;
onClick(){
  this.isFavourite = !this.isFavourite;
}

}
