import {Component} from 'angular2/core'

@Component ({

  selector: 'controllers' ,
  template: `
    <h4> showing list using if statement   </h4>
    <div *ngIf="courses.length > 0" >
      List of courses
    </div>

    <div *ngIf="courses.length == 0" >
      You don't have any courses
    </div>

    <hr>
    <h4> showing list using Switch  </h4>
    <div>
        <ul  class="nav nav-pills" >
          <li [class.active] = "viewMode =='map'"> <a (click)="viewMode = 'map'"> Map view </a></li>
          <li [class.active] = "viewMode =='list'"> <a (click)="viewMode = 'list'"> List view </a></li>
        </ul>
    </div>

    <div [ngSwitch] ="viewMode">
        <template [ngSwitchWhen] ="'map'" ngSwitchDefault> Map view content  </template>
        <template  [ngSwitchWhen] ="'list'"> List view content </template>
    </div>

    <hr>
    <h4> Using pipes and for loop  </h4>
      <ul *ngFor="#course of courses">
          <li> {{ course.title | uppercase }} </li>
          <li> {{ course.rating | number }} </li>
          <li> {{ course.students | number:'2.2-2' }} </li>
          <li> {{ course.price }} </li>
          <li> {{ course.releaseDate | date:'MMM yyyy' }} </li>
          <li> {{ course | json }} </li>


      </ul>

   `

})

export class ControllersComponent {
    courses = [
      {
        title : " Data structure " ,
        rating : 43.32 ,
        students :5435 ,
        price : 99.96 ,
        releaseDate : new Date(2016, 5, 12)

      },
      {
        title : "Logic Design" ,
        rating : 443 ,
        students :523435 ,
        price : 645326.43 ,
        releaseDate : new Date(2016, 3, 10)
      }
    ] ;

    viewMode= "map";

}
