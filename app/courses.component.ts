import {Component} from 'angular2/core';
import {CourseService} from './course.service'
import {AutoGrowDirective} from './auto-grow.directive'
@Component ({
  selector: 'courses',
  template:
  `


  <ul class="list-group">
    <li *ngFor="#course of courses" class="list-group-item">

      {{course}}

    </li>
  </ul>
  <hr />
  <label class="form-label">{{ title }} </label>
  <input type="text" autoGrow class="form-control" />
  `,
  providers : [CourseService],
  directives: [AutoGrowDirective]

})

export class CoursesComponent {
    title = " directive example | auto grow input ";
    courses;
    constructor (courseService : CourseService){
      this.courses = courseService.getCourses();
    }
}
