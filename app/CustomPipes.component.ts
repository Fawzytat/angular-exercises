
 import {Component} from 'angular2/core'
 import {SummaryPipe} from './summary.pipe'

 @Component({
   selector: 'customPipes',
   template: `
      {{ Post.title }}
      <br>
      {{ Post.body | summary:120 }}
   `,
   pipes:[SummaryPipe]
 })

export class CustomPipes {
  Post =
    {
      title : " Summary custom Pipe example returns 120 characters then add ... " ,
      body : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."


    }

}
