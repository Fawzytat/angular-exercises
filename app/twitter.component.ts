import {Component, Input, Output} from 'angular2/core'
import {TweetsService} from './tweets.service'
import {LikeComponent} from './like.component';
@Component({
  selector:'twitter',
  template:
  `
  <div class="media" *ngFor="#tweet of tweets">
  <div class="media-left">
    <a href="#">
      <img class="media-object" src="{{ tweet.imageUrl }}" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading"> {{ tweet.author }} {{ tweet.handle }} </h4>
    {{ tweet.body }}
    <hr />
    <likes [totallikes]="tweet.totalLikes" [ilike]="tweet.iLike"></likes>
  </div>
</div>

  `,
  styles: [
     `

     `
   ]
   ,providers:[TweetsService]
   ,directives: [LikeComponent]
})


export class TwitterComponent {
  tweets;
  constructor (tweetsService : TweetsService){
    this.tweets = tweetsService.getTweets();

  }



}
