import {Component, Input, Output, EventEmitter} from 'angular2/core'

@Component({
  selector:'votes',
  template:`
    <div class="SpanWidth">
    <i class="glyphicon glyphicon-menu-up" [class.highlighted]="myVote == 1" (click)="UpVote()"></i>
    <span class="vote-count"> {{ totalVotes + myVote }} </span>
    <i class="glyphicon glyphicon-menu-down" [class.highlighted]="myVote == -1" (click)="DownVote()"></i>
    </div>
  `,
  styles: [
    `
    .glyphicon-menu-down
    {
      color : #ccc;
      cursor : pointer ;
    }
    .vote-count
    {
      font-size: 1.2em;
    }
    .glyphicon-menu-up
    {
      color : #ccc;
      cursor : pointer ;
    }
    .SpanWidth {
      width:20px;
      text-align:center;
      color:#999;
    }
    .highlighted {
      color : deeppink;
    }
    `
  ]


})

export class VoterComponent {
  @Input() totalVotes = 0 ;
  @Input() myVote = 0 ;
  @Output() vote = new EventEmitter();

  UpVote() {
    if(this.myVote == 1)
    return;
    this.myVote++;

    this.vote.emit({ myVote : this.myVote});
  }

  DownVote() {
    if(this.myVote == -1)
    return;
    this.myVote--;
        this.vote.emit({ myVote : this.myVote});
  }

}
